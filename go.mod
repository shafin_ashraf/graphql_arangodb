module github.com/Shafin098/gql

go 1.16

require (
	github.com/arangodb/go-driver v0.0.0-20210621075908-e7a6fa0cbd18
	github.com/graph-gophers/dataloader/v6 v6.0.0
	github.com/graphql-go/graphql v0.7.9
	github.com/graphql-go/handler v0.2.3
)
