package gql_types

import (
	"context"
	"github.com/Shafin098/gql/db_service"
	"github.com/arangodb/go-driver"
	"github.com/graph-gophers/dataloader/v6"
	"github.com/graphql-go/graphql"
	"log"
)

func DefineTypes(db driver.Database, bookLoader *dataloader.Loader, authorLoader *dataloader.Loader) (*graphql.Object, *graphql.Object) {
	bookType := graphql.NewObject(graphql.ObjectConfig{
		Name: "Book",
		Fields: graphql.Fields{
			"id":   &graphql.Field{Type: graphql.String},
			"name": &graphql.Field{Type: graphql.String},
		},
	})

	authorType := graphql.NewObject(graphql.ObjectConfig{
		Name: "Author",
		Fields: graphql.Fields{
			"id":   &graphql.Field{Type: graphql.String},
			"name": &graphql.Field{Type: graphql.String},
		},
	})

	bookType.AddFieldConfig("author", &graphql.Field{
		Type: authorType,
		Resolve: func(p graphql.ResolveParams) (interface{}, error) {
			thunk := authorLoader.Load(context.TODO(), dataloader.StringKey(p.Source.(db_service.Book).AuthorId))
			// why this works?
			// does resolve is called again and again until authorType is not returned?
			return func() (interface{}, error) {
				return thunk()
			}, nil
		},
	})

	authorType.AddFieldConfig("books", &graphql.Field{
		Type: graphql.NewList(bookType),
		Resolve: func(p graphql.ResolveParams) (interface{}, error) {
			author := p.Source.(db_service.Author)
			bookThunks := bookLoader.LoadMany(context.TODO(), dataloader.NewKeysFromStrings(author.BooksId))

			// why this works?
			// does resolve is called again and again until graphql.NewList(bookType) is not returned?
			return func() (interface{}, error) {
				authorsBooks, err := bookThunks()
				if err != nil {
					log.Fatalf("Can't call book thunk\n err: %v", err)
				}
				return authorsBooks, nil
			}, nil
		},
	})
	return bookType, authorType
}

func DefineRootQuery(db driver.Database, bookType *graphql.Object, authorType *graphql.Object) graphql.ObjectConfig {
	rootQuery := graphql.ObjectConfig{
		Name: "RootQuery",
		Fields: graphql.Fields{
			"author": &graphql.Field{
				Type: authorType,
				Args: graphql.FieldConfigArgument{
					"id": &graphql.ArgumentConfig{Type: graphql.String},
				},
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					author := db_service.GetAuthor(db, p.Args["id"].(string))
					return author, nil
				},
			},
			"authors": &graphql.Field{
				Type: graphql.NewList(authorType),
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					authors := db_service.GetAllAuthors(db)
					return authors, nil
				},
			},
			"book": &graphql.Field{
				Type: bookType,
				Args: graphql.FieldConfigArgument{
					"id": &graphql.ArgumentConfig{Type: graphql.String},
				},
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					book := db_service.GetBook(db, p.Args["id"].(string))
					return book, nil
				},
			},
			"books": &graphql.Field{
				Type: graphql.NewList(bookType),
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					books := db_service.GetAllBooks(db)
					return books, nil
				},
			},
		},
	}

	return rootQuery
}

func DefineMutation(db driver.Database, bookType *graphql.Object) graphql.ObjectConfig {
	mutation := graphql.ObjectConfig{
		Name: "Mutation",
		Fields: graphql.Fields{
			"addBook": &graphql.Field{
				Type: bookType,
				Args: graphql.FieldConfigArgument{
					"id": &graphql.ArgumentConfig{
						Type: graphql.NewNonNull(graphql.String),
					},
					"name": &graphql.ArgumentConfig{
						Type: graphql.NewNonNull(graphql.String),
					},
					"authorId": &graphql.ArgumentConfig{
						Type: graphql.NewNonNull(graphql.String),
					},
				},
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					id := p.Args["id"].(string)
					name := p.Args["name"].(string)
					authorId := p.Args["authorId"].(string)
					newBook := db_service.Book{Id: id, Name: name, AuthorId: authorId}
					db_service.InsertBook(db, newBook)
					authors := db_service.GetAllAuthors(db)
					for i, author := range authors {
						if authorId == author.Id {
							authors[i].BooksId = append(author.BooksId, id)
							db_service.UpdateAuthor(db, authors[i].Id, authors[i].BooksId)
							break
						}
					}
					return newBook, nil
				},
			},
		},
	}

	return mutation
}
